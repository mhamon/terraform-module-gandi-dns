# Module Terraform pour le DNS Gandi

Il s'agit d'un module perso pour mon infra

### How-to
* Regarder dans le dossie example pour les configurations a appliquer dans les fichiers `*.tf`
* Créer le `terraform.auto.tfvars` avec la configuration DNS voulue
* Lancer les commandes en CLI :
```bash
$ terraform init
$ terraform plan 
$ terraform apply
```

data "gandi_domain" "domain_name" {
  name = var.domain_name
}

locals {
  zone = data.gandi_domain.domain_name.id
}

resource "gandi_livedns_record" "domain_mx" {
  count  = var.domain_mx != null ? 1 : 0
  zone   = local.zone
  name   = "@"
  type   = "MX"
  ttl    = var.ttl
  values = var.domain_mx
}

resource "gandi_livedns_record" "domain_caa" {
  count  = var.domain_caa != null ? 1 : 0
  zone   = local.zone
  name   = "@"
  type   = "CAA"
  ttl    = var.ttl
  values = var.domain_caa
}

resource "gandi_livedns_record" "domain_spf" {
  count  = var.domain_spf != null ? 1 : 0
  zone   = local.zone
  name   = "@"
  type   = "SPF"
  ttl    = var.ttl
  values = var.domain_spf
}

resource "gandi_livedns_record" "domain_srv" {
  for_each = var.domain_srv
  zone     = local.zone
  name     = each.value.name
  type     = "SRV"
  ttl      = var.ttl
  values   = [each.value.value]
}

resource "gandi_livedns_record" "domain_root_txt" {
  count  = var.domain_root_txt != null ? 1 : 0
  zone   = local.zone
  name   = "@"
  type   = "TXT"
  ttl    = var.ttl
  values = var.domain_root_txt
}

resource "gandi_livedns_record" "domain_txt" {
  for_each = var.domain_txt
  zone     = local.zone
  name     = each.value.name
  type     = "TXT"
  ttl      = var.ttl
  values   = [each.value.value]
}

resource "gandi_livedns_record" "domain_a" {
  for_each = var.domain_a
  zone     = local.zone
  name     = each.value.name
  type     = "A"
  ttl      = var.ttl
  values   = [each.value.value]
}

resource "gandi_livedns_record" "domain_cname" {
  for_each = var.domain_cname
  zone     = local.zone
  name     = each.value.name
  type     = "CNAME"
  ttl      = var.ttl
  values   = [each.value.value]
}

module "dns" {
  source = "git@gitlab.com:mhamon/terraform-module-gandi-dns.git"

  domain_name = var.domain_name
  GANDI_KEY   = var.GANDI_KEY

  domain_mx       = var.domain_mx
  domain_spf      = var.domain_spf
  domain_txt      = var.domain_txt
  domain_srv      = var.domain_srv
  domain_root_txt = var.domain_root_txt
  domain_a        = var.domain_a
  domain_caa      = var.domain_caa
  domain_cname    = var.domain_cname

}

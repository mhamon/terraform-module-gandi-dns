terraform {

  required_version = ">= 1.4"
  required_providers {
    gandi = {
      source  = "go-gandi/gandi"
      version = ">= 2.2.3"
    }
  }
}

provider "gandi" {
  key = var.GANDI_KEY
}

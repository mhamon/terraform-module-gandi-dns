domain_name = "example.com"

domain_mx = [
  "10 mail.example.com.",
  "50 mail.example.com."
]

domain_srv = {
  first = {
    name  = "first"
    value = "0 0 0   ."
  },
  second = {
    name  = "second"
    value = "0 0 0   ."
  }
}

domain_a = {
  root = {
    name  = "@"
    value = "X.X.X.X"
  }
}

domain_root_txt = [
  "this is a test",
  "another \"example\""
]

domain_txt = {
  one = {
    name  = "one"
    value = "\"this is a test\""
  }
}

domain_cname = {
  www = {
    name  = "www"
    value = "www.example.com."
  },
  webmail = {
    name  = "webmail"
    value = "webmail.example.com."
  }
}

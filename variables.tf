variable "domain_name" {
  description = "Domain name"
  type        = string
  default     = "menguele.com"
}

variable "ttl" {
  description = "TTL"
  type        = number
  default     = 3600
}

variable "domain_mx" {
  description = "MX Records"
  type        = list(any)
  default     = null
}

variable "domain_caa" {
  description = "CAA Records"
  type        = list(any)
  default     = null
}

variable "domain_spf" {
  description = "SPF Records"
  type        = list(any)
  default     = null
}

variable "domain_srv" {
  description = "SRV Records"
  type = map(object({
    name  = string
    value = string
  }))
  default = {}
}

variable "domain_a" {
  description = "A Records"
  type = map(object({
    name  = string
    value = string
  }))
  default = {}
}

variable "domain_root_txt" {
  description = "TXT Root Records"
  type        = list(any)
  default     = null
}

variable "domain_txt" {
  description = "TXT Records"
  type = map(object({
    name  = string
    value = string
  }))
  default = {}
}

variable "domain_cname" {
  description = "CNAME Records"
  type = map(object({
    name  = string
    value = string
  }))
  default = {}
}
